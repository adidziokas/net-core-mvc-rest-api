using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmdAPI.Models;

namespace CmdAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieContext _context;

        public MoviesController(MovieContext context)
        {
            _context = context;
        }
        //GET:      api/movies/
        [HttpGet]
        public ActionResult<IEnumerable<Movie>> GetMovies()
        {
            return _context.MovieItems;
        }
        //GET:      api/movies/n
        [HttpGet("{id}")]
        public ActionResult<Movie> GetMovie(int id)
        {
            var movieItem = _context.MovieItems.Find(id);
            if(movieItem == null)
            {
                return NotFound();
            }
            return movieItem;
        }
        //POST:     api/movies
        [HttpPost]
        public ActionResult<Movie> PostMovie(Movie movie)
        {
            _context.MovieItems.Add(movie);
            _context.SaveChanges();

            return CreatedAtAction("GetMovie", new Movie{Id = movie.Id}, movie);
        }
        //PUT:      api/commands/n
        [HttpPut("{id}")]
        public ActionResult PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            _context.Entry(movie).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
        //DELETE:   api/commands/n
        [HttpDelete("{id}")]
        public ActionResult<Movie> DeleteMovie(int Id)
        {
            var movieItem = _context.MovieItems.Find(Id);
            if(movieItem == null)
            {
                return NotFound();
            }
            _context.MovieItems.Remove(movieItem);
            _context.SaveChanges();
            return movieItem;
        }
    }
}