using Microsoft.EntityFrameworkCore;

namespace CmdAPI.Models
{
    public class MovieContext : DbContext
    {
        public MovieContext(DbContextOptions<MovieContext> options) : base (options)
        {

        }

        public DbSet<Movie> MovieItems {get; set;}
        
    }
}