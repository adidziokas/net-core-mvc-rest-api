namespace CmdAPI.Models
{
    public class Movie
    {
        public int Id {get; set; }
        public int yearReleased{get; set;}
        public string Rating {get; set;}
        public string MovieName {get; set;}
        public string MovieDesc {get; set;}
    }
}