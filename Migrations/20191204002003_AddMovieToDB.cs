﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CmdAPI.Migrations
{
    public partial class AddMovieToDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MovieItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    yearReleased = table.Column<int>(nullable: false),
                    Rating = table.Column<string>(nullable: true),
                    MovieName = table.Column<string>(nullable: true),
                    MovieDesc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieItems");
        }
    }
}
